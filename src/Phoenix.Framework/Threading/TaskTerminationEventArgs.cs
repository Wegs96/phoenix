﻿using System;

namespace Phoenix.Framework.Threading
{
    /// <summary>
    /// The reason for task termination.
    /// </summary>
    public enum TaskTerminationReason
    {
        /// <summary>
        /// The task was terminated by the user.
        /// </summary>
        ByUser,

        /// <summary>
        /// Tick event handler threw an exception.
        /// </summary>
        HandlerThrewException,
    }

    /// <summary>
    /// The handler delegate for task termination events.
    /// </summary>
    /// <param name="sender">The sender task instance.</param>
    /// <param name="e">The event args <see cref="TaskTerminatedEventArgs"/>.</param>
    public delegate void TaskTerminatedEventHandler(IManageableTask sender, TaskTerminatedEventArgs e);

    /// <summary>
    /// The event args for manageable task termination callback.
    /// </summary>
    public sealed class TaskTerminatedEventArgs : EventArgs
    {
        /// <summary>
        /// The reason for task termination.
        /// </summary>
        public TaskTerminationReason Reason { get; private set; }

        /// <summary>
        /// The <see cref="System.Exception"/> instance.
        /// </summary>
        public Exception Exception { get; private set; }

        /// <summary>
        /// Initializes a new instance of <see cref="TaskTerminatedEventArgs"/>.
        /// </summary>
        /// <param name="reason">The reason for task termination.</param>
        public TaskTerminatedEventArgs(TaskTerminationReason reason)
        {
            this.Reason = reason;
            this.Exception = null;
        }

        /// <summary>
        /// Initializes a new instance of <see cref="TaskTerminatedEventArgs"/>.
        /// </summary>
        /// <param name="ex">The <see cref="System.Exception"/> instance.</param>
        public TaskTerminatedEventArgs(Exception ex)
        {
            this.Reason = TaskTerminationReason.HandlerThrewException;
            this.Exception = ex;
        }

        /// <summary>
        /// Prevents initialization of a new instance of <see cref="TaskTerminatedEventArgs"/> if
        /// no valid constructor argument supplied.
        /// </summary>
        private TaskTerminatedEventArgs()
        {
        }
    }
}