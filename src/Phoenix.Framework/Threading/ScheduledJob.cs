﻿using System;

namespace Phoenix.Framework.Threading
{
    /// <summary>
    /// The delegate for scheduled job events.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The event args. See <see cref="ScheduledJobEventArgs"/>.</param>
    public delegate void ScheduledJobEventHandler(ScheduledJob sender, ScheduledJobEventArgs e);

    /// <summary>
    /// The scheduled job implementation.
    /// </summary>
    public class ScheduledJob : IManageableTask
    {
        #region Private & static

        private OverlappedJobManager _overlappedJobMgr;
        private OverlappedJob _workerOverlappedJob;
        private ScheduledJobEventKind _lastState;
        private bool _isDisposed;

        #endregion Private & static

        #region Constructors

        /// <summary>
        /// Initializes a new instance of <see cref="ScheduledJob"/>.
        /// </summary>
        public ScheduledJob() :
            base()
        {
        }

        /// <summary>
        /// The destructor for <see cref="ScheduledJob"/>.
        /// </summary>
        ~ScheduledJob()
        {
            this.Dispose(false);
        }

        /// <summary>
        /// Configures the <see cref="ScheduledJob"/> instance.
        /// </summary>
        /// <param name="overlappedJobMgr">The overlapped job manager.</param>
        /// <param name="firstStartTime">The time when the job starts firing <see cref="OnStateChanged"/> events.</param>
        /// <param name="duration">The job duration.</param>
        /// <param name="isSingleRun">Flag indicating if the job should be only executed once.</param>
        public void Configure(OverlappedJobManager overlappedJobMgr, DateTime firstStartTime, TimeSpan duration, bool isSingleRun)
        {
            _overlappedJobMgr = overlappedJobMgr;
            _lastState = ScheduledJobEventKind.Ended;

            this.FirstStartTime = firstStartTime;
            this.Duration = duration;
            this.IsSingleRun = isSingleRun;

            _workerOverlappedJob = _overlappedJobMgr.CreateJob(false, duration);
            _workerOverlappedJob.Configure(false, duration, false);

            _workerOverlappedJob.OnTick += this.OnOverlappedJobTick;

            //Subscribe to worker termination event handler is done after the configure call, so we just dispatch.
            _workerOverlappedJob.OnTerminated += (s, e) => this.OnTerminated(s, e);
        }

        private void OnOverlappedJobTick(OverlappedJob sender, OverlappedJobTickEventArgs e)
        {
            ScheduledJobEventKind nextState;

            //Next state must be the opposite.
            switch (_lastState)
            {
                case ScheduledJobEventKind.Ended:
                    nextState = ScheduledJobEventKind.Started;
                    break;

                case ScheduledJobEventKind.Started:
                    nextState = ScheduledJobEventKind.Ended;
                    break;

                default:
                    throw new InvalidOperationException("Unknown scheduled job event kind / state.");
            }

            try
            {
                this.OnStateChanged?.Invoke(this, new ScheduledJobEventArgs(nextState));
            }
            catch (Exception ex)
            {
                this.Terminate(TaskTerminationReason.HandlerThrewException, ex);
            }

            _lastState = nextState;

            Console.WriteLine($"New state: {nextState}.");

            //Terminate right after first tick.
            if (this.IsSingleRun)
                this.Terminate(TaskTerminationReason.ByUser);
        }

        #endregion Constructors

        #region ScheduledJob

        /// <summary>
        /// Indicates whether the job should be ran only once, or in intervals.
        /// </summary>
        public bool IsSingleRun { get; private set; }

        /// <summary>
        /// The time when the job should be ran for the first time.
        /// </summary>
        public DateTime FirstStartTime { get; private set; }

        /// <summary>
        /// The job duration.
        /// </summary>
        public TimeSpan Duration { get; private set; }

        /// <summary>
        /// Fired once the job state is changed.
        /// </summary>
        public event ScheduledJobEventHandler OnStateChanged = (s, e) => { };

        #endregion ScheduledJob

        #region IManageableTask

        /// <summary>
        /// The event that gets fired when the task is terminated.
        /// </summary>
        public event TaskTerminatedEventHandler OnTerminated = (s, e) => { };

        /// <summary>
        /// Gets the flag indicating whether the task is currently running.
        /// </summary>
        public bool IsRunning => _workerOverlappedJob.IsRunning;

        /// <summary>
        /// Starts the task execution.
        /// </summary>
        /// <param name="waitForStart">Indicates if the method will block until task is actually started.</param>
        public void Run(bool waitForStart) => _overlappedJobMgr.Run(_workerOverlappedJob, waitForStart);

        /// <summary>
        /// Terminates the task.
        /// </summary>
        /// <param name="reason">The termination reason.</param>
        /// <param name="ex">Exception (if any).</param>
        public void Terminate(TaskTerminationReason reason, Exception ex = null)
        {
            _overlappedJobMgr.Terminate(_workerOverlappedJob, reason, ex);
            _lastState = ScheduledJobEventKind.Ended;
        }

        #endregion IManageableTask

        #region IDisposable

        /// <summary>
        /// The <see cref="IDisposable.Dispose"/> implementation.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The <see cref="IDisposable.Dispose"/> implementation.
        /// </summary>
        /// <param name="disposing"></param>
        public void Dispose(bool disposing)
        {
            if (_isDisposed)
                return;

            if (disposing)
            {
                _workerOverlappedJob?.Dispose();
                _workerOverlappedJob = null;
            }

            _isDisposed = true;
        }

        #endregion IDisposable
    }
}