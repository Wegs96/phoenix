﻿using System;

namespace Phoenix.Framework.Threading
{
    /// <summary>
    /// The scheduled job manager.
    /// </summary>
    public class ScheduledJobManager : TaskManager<ScheduledJob>
    {
        #region Private & static

        private OverlappedJobManager _overlappedJobMgr;
        private OverlappedJob _firstStartEnforcer;

        #endregion Private & static

        /// <summary>
        /// Initializes a new instance of <see cref="ScheduledJobManager"/>.
        /// </summary>
        public ScheduledJobManager()
        {
        }

        /// <summary>
        /// Configures the schedule manager & starts the schedule first startup enforcer.
        ///  <param name="overlappedJobMgr">The overlapped job manager.</param>
        /// <param name="startupPollInterval">The scheduled startup (first run enforcement) interval.</param>
        /// </summary>
        public void Configure(OverlappedJobManager overlappedJobMgr, TimeSpan startupPollInterval)
        {
            _overlappedJobMgr = overlappedJobMgr;
            //Create & register the startup enforcer job, but dont start it yet.
            _firstStartEnforcer = _overlappedJobMgr.CreateJob(false, startupPollInterval);

            _firstStartEnforcer.OnTick += this.OnScheduledJobFirstStartEnforceTick;
            _overlappedJobMgr.Run(_firstStartEnforcer, false);
        }

        private void OnScheduledJobFirstStartEnforceTick(OverlappedJob sender, OverlappedJobTickEventArgs e)
        {
            foreach (var scheduledJob in this.GetTasks())
            {
                if (!scheduledJob.IsRunning && DateTime.Now > scheduledJob.FirstStartTime)
                    this.Run(scheduledJob, false);
            }
        }

        public ScheduledJob CreateJob(DateTime firstStartTime, TimeSpan duration, bool isSingleRun)
        {
            var job = new ScheduledJob();
            job.Configure(_overlappedJobMgr, firstStartTime, duration, isSingleRun);
            base.Register(job);
            return job;
        }
    }
}