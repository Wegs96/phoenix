﻿using System;

namespace Phoenix.Framework.Collections
{
    /// <summary>
    /// Interface for a circular buffer.
    /// </summary>
    /// <typeparam name="T">The type of objects.</typeparam>
    public interface ICircularBuffer<T> : IDisposable
    {
        /// <summary>
        /// Gets a value indicating whether this <see cref="ICircularBuffer{T}"/> is populated.
        /// </summary>
        bool Populated { get; }

        /// <summary>
        /// Gets the count of items in the buffer.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Adds object to the buffer.
        /// </summary>
        /// <param name="obj">The object.</param>
        void Push(in T obj);

        /// <summary>
        /// Cleans the buffer.
        /// </summary>
        void Clear();

        /// <summary>
        /// Gets the buffer snapshot.
        /// </summary>
        /// <returns></returns>
        T[] GetSnapshot();
    }
}
