﻿using System;
using System.Collections.Concurrent;

namespace Phoenix.Framework.Collections
{
    /// <summary>
    /// Circular buffer implementation.
    /// NOTE: .NET Framework does not yet support Clear methods for concurrent collections.
    /// </summary>
    /// <typeparam name="T">The type of objects.</typeparam>
    public class CircularBuffer<T> : ICircularBuffer<T>
    {
        #region Private & static

        private readonly int _capacity;
        private ConcurrentQueue<T> _queue;
        private ConcurrentQueue<BufferAction> _actionQueue;
        private volatile bool _takingSnapshot;
        private bool _disposed;

        #endregion

        #region Constructors & destructor

        /// <summary>
        /// Initializes a new instance of <see cref="CircularBuffer{T}"/>.
        /// </summary>
        /// <param name="capacity">The buffer capacity.</param>
        public CircularBuffer(int capacity)
        {
            _capacity = capacity;
            _queue = new ConcurrentQueue<T>();
            _actionQueue = new ConcurrentQueue<BufferAction>();
            _takingSnapshot = false;
            _disposed = false;
        }

        /// <summary>
        /// Prevents initialization of a new instance of <see cref="CircularBuffer{T}"/>
        /// without a valid constructor argument set supplied.
        /// </summary>
        private CircularBuffer()
        {

        }

        /// <summary>
        /// Destructs an instance of <see cref="CircularBuffer{T}"/>.
        /// </summary>
        ~CircularBuffer()
        {
            this.Dispose(false);
        }

        #endregion

        #region BufferActionType & BufferAction

        /// <summary>
        /// Buffer action type.
        /// </summary>
        private enum BufferActionType
        {
            /// <summary>
            /// Push object to the buffer.
            /// </summary>
            PushObject,

            /// <summary>
            /// Clear the buffer.
            /// </summary>
            Clear,
        }


        /// <summary>
        /// Buffer action scheduled during snapshot operation.
        /// </summary>
        private class BufferAction
        {
            /// <summary>
            /// Gets the buffer action type.
            /// </summary>
            public BufferActionType ActionType { get; private set; }

            /// <summary>
            /// Gets the object.
            /// </summary>
            public T Object { get; private set; }

            /// <summary>
            /// Initializes a new instance of <see cref="BufferAction"/>.
            /// </summary>
            /// <param name="actionType">The action type.</param>
            public BufferAction(BufferActionType actionType)
            {
                if (actionType == BufferActionType.PushObject)
                    throw new ArgumentException(
                        "This action type requires object.");

                this.ActionType = actionType;
            }

            public BufferAction(BufferActionType actionType, T obj)
            {
                if (actionType == BufferActionType.Clear)
                    throw new ArgumentException(
                        "This action type does not require object.");

                this.ActionType = actionType;
                this.Object = obj;
            }

            /// <summary>
            /// Prevents initialization of a new instance of <see cref="BufferAction"/>
            /// without a valid constructor argument set supplied.
            /// </summary>
            private BufferAction()
            {

            }
        }

        #endregion

        #region ICircularBuffer

        /// <inheritdoc />
        public bool Populated => _queue.Count == _capacity;

        /// <inheritdoc />
        public int Count => _queue.Count;

        /// <inheritdoc />
        public void Push(in T obj)
        {
            if (_takingSnapshot)
            {
                _actionQueue.Enqueue(
                    new BufferAction(BufferActionType.PushObject, obj)
                    );

                return;
            }

            //Remove oldest.
            if (this.Populated)
                _queue.TryDequeue(out _);

            _queue.Enqueue(obj);
        }

        /// <inheritdoc />
        public void Clear()
        {
            if (_takingSnapshot)
            {
                _actionQueue.Enqueue(
                    new BufferAction(BufferActionType.Clear)
                );

                return;
            }

            _queue.Clear();
        }

        /// <inheritdoc />
        public T[] GetSnapshot()
        {
            _takingSnapshot = true;

            var result = new T[this.Count];

            int index = 0;
            foreach (var obj in _queue)
                result[index++] = obj;

            _takingSnapshot = false;

            foreach (var action in _actionQueue)
            {
                switch (action.ActionType)
                {
                    case BufferActionType.PushObject:
                        this.Push(action.Object);
                        break;
                    case BufferActionType.Clear:
                        this.Clear();
                        break;
                    default:
                        throw new InvalidOperationException(
                   $"Unknown action type: {action.ToString()}."
                   );
                }
            }

            _actionQueue.Clear();

            return result;
        }

        #endregion

        #region IDisposable

        /// <summary>
        /// Releases all resources used by the current instance of <see cref="BinaryStream"/> class.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases all resources used by the current instance of <see cref="CircularBuffer{T}"/> class.
        /// </summary>
        /// <param name="disposing"><b>true</b> to release both managed and unmanaged resource,
        /// <b>false</b> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _queue?.Clear();
                _actionQueue?.Clear();

                _queue = null;
                _actionQueue = null;
            }

            _disposed = true;
        }

        #endregion
    }
}
