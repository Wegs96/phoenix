﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace Phoenix.Framework.Collections
{
    /// <summary>
    /// Binary stream implementation.
    /// Thanks to Daxter for the original impl with Span/Memory.
    /// Note that .NET framework does not have support for memory marshal, unmanaged keyword
    /// and CreateSpan (at least as far as I know).
    /// </summary>
    public class BinaryStream : IBinaryStream
    {
        #region Private & static

        private MemoryStream _memoryStream;
        private BinaryReader _reader;
        private BinaryWriter _writer;
        private bool _disposed;

        #endregion

        #region Constructors & destructor

        /// <summary>
        /// Initializes a new instance of <see cref="BinaryStream"/>.
        /// The stream is in write mode by default.
        /// </summary>
        public BinaryStream()
        {
            _memoryStream = new MemoryStream();
            _reader = null;
            _writer = new BinaryWriter(_memoryStream);
            _disposed = false;

            this.Mode = BinaryStreamMode.Write;
        }

        /// <summary>
        /// Initializes a new instance of <see cref="BinaryStream"/>.
        /// </summary>
        /// <param name="memory">The memory.</param>
        public BinaryStream(ReadOnlyMemory<byte> memory)
        {
            _memoryStream = new MemoryStream(memory.ToArray());
            _reader = new BinaryReader(_memoryStream);
            _writer = null;
            _disposed = false;

            this.Mode = BinaryStreamMode.Read;
        }

        /// <summary>
        /// Destructs an instance of <see cref="CircularBuffer{T}"/>.
        /// </summary>
        ~BinaryStream()
        {
            this.Dispose(false);
        }

        #endregion

        #region IBinaryStream

        /// <inheritdoc />
        public BinaryStreamMode Mode { get; private set; }

        /// <inheritdoc />
        public int Length => (int)(_memoryStream.Length);

        /// <inheritdoc />
        public int Offset => (int)(_memoryStream.Position);


        /// <inheritdoc />
        public void SwitchMode(BinaryStreamMode mode)
        {
            if (this.Mode == mode)
                return;

            //Must be set before the seek call, otherwise it will fail.
            this.Mode = mode;

            if (mode == BinaryStreamMode.Read)
            {
                if (_reader == null)
                    _reader = new BinaryReader(_memoryStream);
            }

            if (mode == BinaryStreamMode.Write)
            {
                if (_writer == null)
                    _writer = new BinaryWriter(_memoryStream);
            }
        }

        /// <inheritdoc />
        public void Seek(int offset, SeekOrigin seekOrigin = SeekOrigin.Begin)
        {
            if (offset > this.Length)
                throw new IndexOutOfRangeException("Cannot seek outside stream bounds.");

            _memoryStream.Seek(offset, seekOrigin);
        }

        /// <inheritdoc />
        public T Read<T>()
            where T : unmanaged
        {
            if (this.Mode != BinaryStreamMode.Read)
                throw new InvalidOperationException("The stream is not in read mode.");

            if (Marshal.SizeOf(typeof(T)) > this.Length)
                throw new InvalidOperationException("Cannot read outside the stream bounds.");

            var value = default(T);
            var span = MemoryMarshal.CreateSpan(ref value, 1);

            _memoryStream.Read(MemoryMarshal.Cast<T, byte>(span));

            return value;
        }

        /// <inheritdoc />
        public void ReadArray<T>(in Span<T> destination)
            where T : unmanaged
        {
            int totalSize = Marshal.SizeOf(typeof(T)) * destination.Length;
            if (totalSize > this.Length)
                throw new InvalidOperationException("Cannot read outside the stream bounds.");

            if (this.Mode != BinaryStreamMode.Read)
                throw new InvalidOperationException("The stream is not in read mode.");

            _memoryStream.Read(MemoryMarshal.Cast<T, byte>(destination));
        }

        /// <inheritdoc />
        public void ReadArray<T>(in Memory<T> destination)
            where T : unmanaged => this.ReadArray<T>(destination.Span);

        /// <inheritdoc />
        public T[] ReadArray<T>(int count)
            where T : unmanaged
        {
            int totalSize = Marshal.SizeOf(typeof(T)) * count;

            if (totalSize > this.Length)
                throw new InvalidOperationException("Cannot read outside the stream bounds.");

            if (this.Mode != BinaryStreamMode.Read)
                throw new InvalidOperationException("The stream is not in read mode.");

            var values = new T[count];
            this.ReadArray(values.AsSpan());
            return values;
        }

        /// <inheritdoc />
        public void Write<T>(ref T value)
            where T : unmanaged
        {
            if (this.Mode != BinaryStreamMode.Write)
                throw new InvalidOperationException("The stream is not in write mode.");

            var span = MemoryMarshal.CreateReadOnlySpan(ref value, 1);
            _writer.Write(MemoryMarshal.Cast<T, byte>(span));
        }

        /// <inheritdoc />
        public void Write<T>(T value)
            where T : unmanaged => this.Write(ref value);

        /// <inheritdoc />
        public void WriteArray<T>(in ReadOnlySpan<T> source)
            where T : unmanaged
        {
            if (this.Mode != BinaryStreamMode.Write)
                throw new InvalidOperationException("The stream is not in write mode.");

            _writer.Write(MemoryMarshal.Cast<T, byte>(source));
        }

        /// <inheritdoc />
        public void WriteArray<T>(in ReadOnlyMemory<T> source)
            where T : unmanaged => this.WriteArray<T>(source.Span);

        /// <inheritdoc />
        public void WriteArray<T>(in T[] source, int offset, int count)
            where T : unmanaged
        {
            if ((offset + count) > source.Length)
                throw new IndexOutOfRangeException("Offset / count above of source array length.");

            this.WriteArray<T>(source.AsSpan(offset, count));
        }

        #endregion

        #region IDisposable

        /// <summary>
        /// Releases all resources used by the current instance of <see cref="BinaryStream"/> class.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases all resources used by the current instance of <see cref="BinaryStream"/> class.
        /// </summary>
        /// <param name="disposing"><b>true</b> to release both managed and unmanaged resource,
        /// <b>false</b> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _reader?.Dispose();
                _writer?.Dispose();
                _memoryStream?.Dispose();

                _reader = null;
                _writer = null;
                _memoryStream = null;
            }

            _disposed = true;
        }

        #endregion
    }
}
