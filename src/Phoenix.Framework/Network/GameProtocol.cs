﻿using Phoenix.Framework.Collections;
using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Phoenix.Framework.Network
{
    /// <summary>
    /// Game protocol core class. Encrypted packets are currently unsupported.
    /// </summary>
    public class GameProtocol : IGameProtocol
    {
        private readonly CancellationTokenSource _cancellationTokenSource;

        private readonly ConcurrentQueue<ReadOnlySequence<byte>> _decodeQueue;
        private readonly ConcurrentQueue<PacketStream> _incomingQueue;

        private readonly ConcurrentQueue<PacketStream> _encodeQueue;
        private readonly ConcurrentQueue<BinaryStream> _outgoingQueue;

        private BinaryStream _tmpStream;
        private ushort _packetDataSize;
        private bool _packetDataSizeReceived;
        private PacketStream _massivePacket;
        private ushort _massiveCount;

        private const int LengthPrefixSize = 2;
        private const int HeaderSize = 6;

        /// <summary>
        /// initializes a new instance of <see cref="GameProtocol"/>.
        /// </summary>
        public GameProtocol()
        {
            _cancellationTokenSource = new CancellationTokenSource();

            _tmpStream = new BinaryStream();
            _decodeQueue = new ConcurrentQueue<ReadOnlySequence<byte>>();
            _incomingQueue = new ConcurrentQueue<PacketStream>();
            _encodeQueue = new ConcurrentQueue<PacketStream>();
            _outgoingQueue = new ConcurrentQueue<BinaryStream>();
        }


        /// <inheritdoc />
        public async Task DecodePacketsAsync(ReadOnlySequence<byte> data)
        {
            long remainRead = data.Length;

            do
            {
                //Receive the packet size.
                if (!_packetDataSizeReceived)
                {
                    ReadOnlySequence<byte> sizeBuffer = data.Slice(data.Start, LengthPrefixSize);
                    _packetDataSize = BitConverter.ToUInt16(sizeBuffer.ToArray(), 0);
                    _packetDataSizeReceived = true;
                }

                //Write data to the temporal stream if packet is not yet fully received.
                long expectedRemainBytes = ((_packetDataSize + HeaderSize) - _tmpStream.Length);
                if (_tmpStream.Length != expectedRemainBytes)
                {
                    ReadOnlySequence<byte> chunk = data;
                    if (data.Length >= expectedRemainBytes)
                        chunk = data.Slice(data.Start, expectedRemainBytes);

                    var segmentPosition = chunk.Start;
                    while (chunk.TryGet(ref segmentPosition, out ReadOnlyMemory<byte> memory, true))
                        _tmpStream.WriteArray(memory);

                    remainRead -= chunk.Length;
                }

                if (_tmpStream.Length == (_packetDataSize + HeaderSize))
                {
                    //The temporal stream size matched the excepted size, so the packet is completely received 
                    //and we need to put it to the incoming queue for further processing.
                    _tmpStream.SwitchMode(BinaryStreamMode.Read);
                    _tmpStream.Seek(0, SeekOrigin.Begin);

                    if (remainRead != 0)
                        data = data.Slice(expectedRemainBytes, remainRead);

                    ushort payloadSize = _tmpStream.Read<ushort>();
                    ushort opcode = _tmpStream.Read<ushort>();
                    ushort security = _tmpStream.Read<ushort>();
                    byte[] body = new byte[0];

                    if (opcode == 0x600D)
                    {
                        bool isHeader = _tmpStream.Read<bool>();
                        if (isHeader)
                        {
                            this._massiveCount = _tmpStream.Read<ushort>();
                            ushort massiveOpcode = _tmpStream.Read<ushort>();
                            this._massivePacket = new PacketStream(massiveOpcode, false, true);
                        }
                        else
                        {
                            if (this._massivePacket == null)
                            {
                                // throw Exception!
                            }
                            this._massivePacket.WriteArray<byte>(new ReadOnlyMemory<byte>(_tmpStream.ReadArray<byte>(_tmpStream.Length - 1)));
                            this._massiveCount--;
                            if (this._massiveCount == 0)
                            {
                                this._massivePacket.SwitchMode(BinaryStreamMode.Read);
                                this._massivePacket.Seek(0, SeekOrigin.Begin);
                                _incomingQueue.Enqueue(this._massivePacket);
                                this._massivePacket = null;
                            }
                        }
                    }
                    else
                    {
                        body = _tmpStream.ReadArray<byte>(payloadSize);

                        //Create packet stream.
                        var packet = new PacketStream(opcode);
                        packet.WriteArray<byte>(body.AsSpan(0, body.Length));
                        packet.SwitchMode(BinaryStreamMode.Read);
                        packet.Seek(0, SeekOrigin.Begin);

                        _incomingQueue.Enqueue(packet);
                    }

                    //Prepare to decode next packet.
                    _tmpStream?.Dispose();
                    _tmpStream = new BinaryStream();
                    _packetDataSizeReceived = false;
                }
            }
            while (remainRead > 0 && !_cancellationTokenSource.IsCancellationRequested);
        }

        /// <inheritdoc />
        public async Task EncodePacketsAsync()
        {
            foreach (var packet in _encodeQueue)
            {
                if (packet.Massive)
                {
                    var massiveHeader = new PacketStream(0x600D);
                    massiveHeader.Write<byte>(0x01);
                    massiveHeader.Write<byte>(0x01);
                    massiveHeader.Write<ushort>(packet.Opcode);

                    var massiveBody = new PacketStream(0x600D);
                    massiveBody.Write<byte>(0x00);
                    massiveBody.WriteArray<byte>(new ReadOnlyMemory<byte>(packet.GetBytes()));

                    _outgoingQueue.Enqueue(massiveHeader.ToBinaryStream());
                    _outgoingQueue.Enqueue(massiveBody.ToBinaryStream());
                }
                else
                {
                    _outgoingQueue.Enqueue(packet.ToBinaryStream());
                }

            }


            _encodeQueue.Clear();
        }

        /// <inheritdoc />
        public void SendPacket(PacketStream packet) => _encodeQueue.Enqueue(packet);

        /// <inheritdoc />
        public ConcurrentQueue<PacketStream> GetIncoming() => _incomingQueue;

        /// <inheritdoc />
        public ConcurrentQueue<BinaryStream> GetOutgoing() => _outgoingQueue;
    }
}
