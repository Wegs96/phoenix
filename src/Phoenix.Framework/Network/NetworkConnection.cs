﻿using System;
using System.Buffers;
using System.IO.Pipelines;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Phoenix.Framework.Network
{
    /// <summary>
    /// Network connection implementation.
    /// Client->Server connections can reconnect.
    /// </summary>
    public class NetworkConnection : INetworkConnection
    {
        #region Private & static

        private Socket _socket;
        private readonly Pipe _recvPipe;
        private readonly Pipe _sendPipe;
        private CancellationTokenSource _cancellationTokenSource;
        private bool _terminated;
        private volatile bool _flushingOutgoing;
        private bool _disposed;


        #endregion

        #region Constructors & destructor

        /// <summary>
        /// Initializes a new instance of <see cref="NetworkConnection"/>.
        /// </summary>
        public NetworkConnection(Socket socket)
        {
            _socket = socket;
            _recvPipe = new Pipe();
            _sendPipe = new Pipe();

            _cancellationTokenSource = new CancellationTokenSource();
            _terminated = false;
            _flushingOutgoing = false;
            _disposed = false;
        }

        /// <summary>
        /// Destructs an instance of <see cref="NetworkConnection"/>.
        /// </summary>
        ~NetworkConnection()
        {
            this.Dispose(false);
        }

        #endregion

        #region INetworkConnection

        /// <inheritdoc />
        public bool Connected { get; private set; } = false;

        /// <inheritdoc />
        public DateTime LastActivity { get; private set; } = DateTime.MinValue;

        /// <inheritdoc />
        public DateTime LastDataProcessTime { get; private set; } = DateTime.MinValue;


        /// <inheritdoc />
        public event DataReceivedEventHandler DataReceived = (s, e) => { };

        /// <inheritdoc />
        public event ConnectionTerminatedEventHandler Terminated = (s, e) => { };

        /// <inheritdoc />
        public async Task StartReceivingData()
        {
            if (_terminated)
                throw new InvalidOperationException("Cannot start receiving data from a terminated connection.");

            try
            {
                //Start filling the pipe.
                while (!_cancellationTokenSource.IsCancellationRequested)
                {
                    Memory<byte> buffer = _recvPipe.Writer.GetMemory(sizeHint: 512);


                    int recvCount = await _socket.ReceiveAsync(buffer, SocketFlags.None, _cancellationTokenSource.Token)
                        .ConfigureAwait(continueOnCapturedContext: false);

                    if (recvCount == 0)
                    {
                        this.Terminate(ConnectionTerminationReason.ReceiveDataFailed);
                        break;
                    }

                    _recvPipe.Writer.Advance(recvCount);

                    this.DataReceived?.Invoke(this, buffer.Slice(0, recvCount));

                    this.LastActivity = DateTime.Now;
                    this.LastDataProcessTime = DateTime.Now;

                    FlushResult result = await _recvPipe.Writer.FlushAsync(_cancellationTokenSource.Token)
                        .ConfigureAwait(continueOnCapturedContext: false);

                    if (result.IsCompleted)
                        break;
                }

                _recvPipe.Writer.Complete();
            }
            catch (Exception ex)
            {
                this.Terminate(ConnectionTerminationReason.ReceiveDataFailed, ex);
            }
        }

        /// <inheritdoc />
        public async Task SendData(ReadOnlyMemory<byte> data, bool flush = false)
        {
            if (_terminated)
                throw new InvalidOperationException("Cannot send data to a terminated connection.");

            try
            {
                FlushResult result = await _sendPipe.Writer.WriteAsync(data, _cancellationTokenSource.Token)
                    .ConfigureAwait(continueOnCapturedContext: false);

                if (flush)
                    await this.FlushOutgoing();
            }
            catch (Exception ex)
            {
                this.Terminate(ConnectionTerminationReason.SendDataFailed, ex);
            }

            this.LastActivity = DateTime.Now;
        }

        /// <inheritdoc />
        public async Task FlushOutgoing()
        {
            if (_terminated)
                throw new InvalidOperationException("Cannot flush data to a terminated connection.");

            //Check whether another flush operation is currently running.
            //Not really sure if this is hackish, or supposed to be this way... anyway, that solves the 
            //Concurrent IO exception being thrown...
            if (_flushingOutgoing)
                return;

            try
            {
                _flushingOutgoing = true;

                //FlushAsync flushes *all* buffers at once, no need to handle this.
                FlushResult writerFlushResult = await _sendPipe.Writer.FlushAsync(_cancellationTokenSource.Token)
                    .ConfigureAwait(continueOnCapturedContext: false);

                ReadResult readResult = await _sendPipe.Reader.ReadAsync(_cancellationTokenSource.Token)
                    .ConfigureAwait(continueOnCapturedContext: false);

                int sentBytes = 0;

                ReadOnlySequence<byte> buffer = readResult.Buffer;
                SequencePosition endPosition = buffer.End;

                while (sentBytes < readResult.Buffer.Length && !readResult.IsCompleted &&
                       !_cancellationTokenSource.IsCancellationRequested)
                {
                    SequencePosition startPosition = buffer.Start;
                    buffer.TryGet(ref startPosition, out ReadOnlyMemory<byte> memory, advance: false);

                    //Send the data to socket.
                    sentBytes += await _socket.SendAsync(memory, SocketFlags.None, _cancellationTokenSource.Token)
                        .ConfigureAwait(continueOnCapturedContext: false);

                    buffer = buffer.Slice(startPosition, buffer.GetPosition(memory.Length, startPosition));

                    this.LastActivity = DateTime.Now;
                }

                _sendPipe.Reader.AdvanceTo(endPosition, endPosition);

                _flushingOutgoing = false;
            }
            catch (Exception ex)
            {
                this.Terminate(ConnectionTerminationReason.FlushOutgoingFailed, ex);
            }
        }


        /// <inheritdoc />
        public void Terminate()
        {
            if (_terminated)
                return;

            _cancellationTokenSource?.Cancel();
            _recvPipe?.Reset();
            _sendPipe?.Reset();
            _terminated = true;

            this.Connected = false;

            this.Terminated?.Invoke(this,
                new ConnectionTerminatedEventArgs(ConnectionTerminationReason.TerminatedByUser));
        }

        /// <summary>
        /// Terminates the connection.
        /// </summary>
        /// <param name="reason">The termination reason.</param>
        /// <param name="ex">The exception (if any).</param>
        private void Terminate(ConnectionTerminationReason reason, Exception ex = null)
        {
            if (_terminated)
                return;

            _cancellationTokenSource?.Cancel();
            _recvPipe?.Reset();
            _sendPipe?.Reset();
            _terminated = true;

            this.Connected = false;

            this.Terminated?.Invoke(this,
                new ConnectionTerminatedEventArgs(reason, ex));
        }

        #endregion

        #region IDisposable

        /// <summary>
        /// Releases all resources used by the current instance of <see cref="NetworkConnection"/> class.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }


        /// <summary>
        /// Releases all resources used by the current instance of <see cref="NetworkConnection"/> class.
        /// </summary>
        /// <param name="disposing"><b>true</b> to release both managed and unmanaged resource,
        /// <b>false</b> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _socket?.Dispose();
                _cancellationTokenSource?.Dispose();

                _socket = null;
                _cancellationTokenSource = null;
            }

            _disposed = true;
        }

        #endregion
    }
}
