﻿using System;
using System.Threading.Tasks;

namespace Phoenix.Framework.Network
{
    /// <summary>
    /// Data received event delegate.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="data">The data.</param>
    public delegate void DataReceivedEventHandler(INetworkConnection sender, ReadOnlyMemory<byte> data);

    /// <summary>
    /// Connection state change event delegate.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The event args.</param>
    public delegate void ConnectionTerminatedEventHandler(INetworkConnection sender, ConnectionTerminatedEventArgs e);


    /// <summary>
    /// Interface for a network connection.
    /// </summary>
    public interface INetworkConnection : IDisposable
    {
        /// <summary>
        /// Gets the flag indicating whether the underlying socket is connected.
        /// </summary>
        bool Connected { get; }

        /// <summary>
        /// Gets last network IO time. Used for timeout / hang detection.
        /// The initial value is set to DateTime.MinValue.
        /// </summary>
        DateTime LastActivity { get; }

        /// <summary>
        /// Gets the time <see cref="DataReceived"/> handlers were completed.
        /// This is used to detect lags / hangs @ data processing logic.
        /// The initial value is set to DateTime.MinValue.
        /// </summary>
        DateTime LastDataProcessTime { get; }

        /// <summary>
        /// Incoming data handlers. Fired once data chunk has been received from the socket.
        /// </summary>
        event DataReceivedEventHandler DataReceived;

        /// <summary>
        /// Fired once the connection was terminated.
        /// </summary>
        event ConnectionTerminatedEventHandler Terminated;

        /// <summary>
        /// Starts receiving data from the socket.
        /// </summary>
        /// <returns>The task.</returns>
        Task StartReceivingData();

        /// <summary>
        /// Enqueues / sends (if the flush flag is set) data to the socket.
        /// Normally flushing should be done by an explicit <see cref="FlushOutgoing"/> call.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="flush">The flag indicating whether data has to be flushed right away.</param>
        /// <returns>The task.</returns>
        Task SendData(ReadOnlyMemory<byte> data, bool flush = false);

        /// <summary>
        /// Flushes pending outgoing data to the socket.
        /// </summary>
        /// <returns>The task.</returns>
        Task FlushOutgoing();

        /// <summary>
        /// Terminates the connection.
        /// </summary>
        void Terminate();
    }
}
