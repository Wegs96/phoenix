﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Security;
using System.Threading;
using System.Threading.Tasks;

namespace Phoenix.Framework.Network
{
    /// <summary>
    /// Socket listener implementation.
    /// </summary>
    public class AsyncSocketListener : ISocketListener
    {
        #region Private & static

        private readonly IPEndPoint _endPoint;
        private Socket _listener;
        private CancellationTokenSource _cancellationToken;
        private bool _disposed;

        #endregion Private & static

        #region Constructors & destructor

        /// <summary>
        /// Initializes a new instance of <see cref="AsyncSocketListener"/>.
        /// </summary>
        /// <param name="endPoint">The local endpoint.</param>
        public AsyncSocketListener(IPEndPoint endPoint)
        {
            _endPoint = endPoint;
            _listener = null;
            _cancellationToken = new CancellationTokenSource();
            _disposed = false;
        }


        /// <summary>
        /// Destructs an instance of <see cref="AsyncSocketListener"/>.
        /// </summary>
        ~AsyncSocketListener()
        {
            this.Dispose(false);
        }

        #endregion

        #region AsyncSocketListener

        private async Task AcceptClientConnectionsAsync()
        {
            while (!_cancellationToken.IsCancellationRequested)
            {
                Socket client = null;

                try
                {
                    client = await _listener.AcceptAsync().
                        ConfigureAwait(continueOnCapturedContext: false);
                }
                catch (Exception ex)
                {
                    //TODO: Log
                }

                try
                {
                    if (this.DispatchAcceptedSockets)
                        this.SocketAccepted?.Invoke(this, client);
                    else client.CloseNoThrow();
                }
                catch (Exception ex)
                {
                    //TODO: Log
                }
            }
        }

        #endregion

        #region ISocketListener

        /// <inheritdoc />
        public bool Running { get; private set; } = false;

        /// <inheritdoc />
        public bool DispatchAcceptedSockets { get; set; } = false;

        /// <inheritdoc />
        public event SocketAcceptedEventHandler SocketAccepted = (s, e) => { };

        /// <inheritdoc />
        public SocketListenerStartupResult Start()
        {
            _listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try { _listener.Bind(_endPoint); }
            catch (SecurityException)
            { return SocketListenerStartupResult.InsufficientPrivileges; }
            catch (Exception)
            { return SocketListenerStartupResult.BindGeneralFailure; }

            try { _listener.Listen(5); }
            catch (SocketException)
            { return SocketListenerStartupResult.ListenGeneralFailure; }

            _ = AcceptClientConnectionsAsync();

            this.Running = true;
            return SocketListenerStartupResult.Success;
        }

        /// <inheritdoc />
        public void Terminate()
        {
            _cancellationToken.Cancel();
            _listener.CloseNoThrow();

            this.Running = false;
        }

        #endregion

        #region IDisposable

        /// <summary>
        /// Releases all resources used by the current instance of <see cref="AsyncSocketListener"/> class.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases all resources used by the current instance of <see cref="AsyncSocketListener"/> class.
        /// </summary>
        /// <param name="disposing"><b>true</b> to release both managed and unmanaged resource,
        /// <b>false</b> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _listener?.Dispose();
                _cancellationToken.Dispose();

                _listener = null;
                _cancellationToken = null;
            }

            _disposed = true;
        }

        #endregion
    }
}